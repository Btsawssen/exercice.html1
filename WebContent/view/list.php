﻿<!-- view/list.php -->

<?php $titre = 'PHP MVC'; ?>

<?php ob_start() ?>
	<h2>Liste des articles</h2>
	<ul>
	<?php foreach($articles as $article){ ?>
		<li>
			<a href="show.php?id=<?php echo $article['id']; ?>">
				<?php echo $article['titre']; ?>
			</a>
		</li>
	<?php } ?>
	</ul>
<?php $content = ob_get_clean(); ?>
<?php include 'layout.php'; ?>