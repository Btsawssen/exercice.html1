﻿<!-- view/show.php -->

<?php $titre = 'Détail'; ?>

<?php ob_start() ?>
	<h2><?php echo $article['titre']; ?></h2>
	<div>
		<?php echo $article['contenu']; ?>
	</div>
	<div><?php echo $article['date_creation']; ?></div>

<?php $content = ob_get_clean(); ?>
<?php include 'layout.php'; ?>