# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.20-log
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2013-06-23 18:47:51
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for testmvc
CREATE DATABASE IF NOT EXISTS `testmvc` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `testmvc`;


# Dumping structure for table testmvc.article
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(10) NOT NULL DEFAULT '0',
  `titre` varchar(50) DEFAULT NULL,
  `contenu` text,
  `auteur` varchar(50) DEFAULT NULL,
  `date_creation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table testmvc.article: ~2 rows (approximately)
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` (`id`, `titre`, `contenu`, `auteur`, `date_creation`) VALUES
	(1, 'titre1', 'contenu1', 'auteur1', '2012/04/22'),
	(2, 'titre2', 'contenu2', 'auteur2', '2013/06/12');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
